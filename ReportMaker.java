package com.example.Raport;

public class ReportMaker implements Visitor {
	
	private int numberOfClients;
	private int numberOfOrders;
	private int numberOfProducts;
	private String report;

	@Override
	public void visit(Visitable v) {
		this.report = "\n" + v.giveReport();
		
		switch(v.getClass().getSimpleName()){
			case "Client.":
				this.numberOfClients++;
				break;
			case "Order." :
				this.numberOfOrders++;
				break;
			case "Product." :
				this.numberOfProducts++;
				break;
		}
	}

	public int getNumberOfClients() {
		return numberOfClients;
	}

	public int getNumberOfOrders() {
		return numberOfOrders;
	}

	public int getNumberOfProducts() {
		return numberOfProducts;
	}
	
	public String showReport(){
		return this.report;
	}
}
