package com.example.Raport;

import java.util.Date;
import java.util.List;

public class Order implements Visitable {
	private String number;
	private double orderTotalPrice;
	private Date orderDate;
	private List<Product> productList;
	
	public Order(String number,List<Product> list){
		this.number =number;
		this.productList = list;
		
		this.orderDate = new Date();
		
		if(productList != null)
        for(Product product : productList)
            this.orderTotalPrice += product.getPrice();
	}

	public String getNumber() {
		return number;
	}

	public double getOrderTotalPrice() {
		return orderTotalPrice;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void addProduct(Product product) {
		this.productList.add(product);
		this.orderTotalPrice += product.getPrice();
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String giveReport() {
		return this.toString();
	}

	public String toString() {
		StringBuilder string = new StringBuilder();

		string.append("Order: ").append(this.number).append(", date: ").append(this.orderDate).append(", price: ")
				.append(this.orderTotalPrice).append("zł .\n Products: ");
		for (Product product : this.productList)
			string.append("\n\t").append(product.toString());
		return string.toString();
	}
}
