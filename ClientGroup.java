package com.example.Raport;

import java.util.ArrayList;
import java.util.List;

public class ClientGroup implements Visitable {
	private String clientName;
	List<Client> clientList;

	public ClientGroup(String name) {
		this.clientName = name;
		this.clientList = new ArrayList<>();
	}

	public void addClient(Client client) {
		this.clientList.add(client);
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String giveReport() {
		return this.toString();
	}

	public String toString() {
		StringBuilder string = new StringBuilder();
		string.append("Client Group: ").append(this.clientName).append("\n").append("\t Clients: \n");
		for (Client client : this.clientList)
			string.append("\t").append(client.toString()).append("\n");

		return string.toString();
	}

}
