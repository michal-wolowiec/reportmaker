package com.example.Raport;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Product prod1 = new Product("Piwo", 2.5);
		Product prod2 = new Product("Wino", 16.89);
		
		
		
		ReportMaker repmak = new ReportMaker();
		repmak.visit(prod1);
		
		List<Product> list = new ArrayList<>();
		list.add(prod1);
		list.add(prod2);
		
		Order ord1 = new Order("Some Order: ", list);
		
		repmak.visit(ord1);
		System.out.println(repmak.showReport());
	}

}
