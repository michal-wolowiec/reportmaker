package com.example.Raport;

import java.util.List;

public class Client implements Visitable {
	private String number;
	List<Order> orderList;

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);

	}

	@Override
	public String giveReport() {

		return this.toString();
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void addOrder(Order order) {
		this.orderList.add(order);
	}

	public String toString() {
		StringBuilder string = new StringBuilder();

		string.append("Client: ").append(this.number).append(". \n \t Orders: \n");
		for (Order order : this.orderList)
			string.append("\t ").append(order.toString()).append("\n");

		return string.toString();
	}
}
