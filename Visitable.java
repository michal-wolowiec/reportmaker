package com.example.Raport;

public interface Visitable {
	public void accept(Visitor visitor);
	
	public String giveReport();
	
}
